## What is Spring.NET?

Spring.NET is an open source application framework that makes building enterprise .NET applications easier. Spring.NET provides comprehensive infrastructural support for developing enterprise .NET applications. It allows you to remove incidental complexity when using the base class libraries makes best practices, such as test driven development, easy practices ...
[Continue reading →](https://asposespringnet.codeplex.com/wikipage?title=What%20is%20Spring.NET)

We have extended two examples of Spring.NET using Aspose File Format APIs

## Spring.NET e-ticket generation in SpringAir using Aspose.Words

SpringAir is a flight reservation and ticket booking system based on Spring.NET that demonstrates the ASP.NET framework showing features such as Dependency Injection for ASP.NET pages, Bi-directional data binding, validation and localization ...
[Continue reading →](https://asposespringnet.codeplex.com/wikipage?title=Spring.NET%20E-ticket%20Generation%20in%20SpringAir%20using%20Aspose.Words%20for%20.NET)

* [How to extend SpringAir to support E-ticket Generation?](https://asposespringnet.codeplex.com/wikipage?title=E-ticket%20Generation%20in%20SpringAir)

## Spring.NET NHibernate Northwind reports using Aspose.Cells and Aspose.Pdf

Spring.NET NHibernate Northwind sample demonstrates use of Spring’s NHibernate integration to simplify the use of NHibernate. Web tier is also included showing how to use the Open-Session In View approach to session management in the web tier ... [Continue reading →](https://asposespringnet.codeplex.com/wikipage?title=Spring.NET%20NHibernate%20Northwind%20Reports%20using%20Aspose.Cells%20and%20Aspose.Pdf)

## How To's
Please check the links below for details instructions for creating different reports

* [How to extend Spring.Northwind to generate different Northwind reports?](https://asposespringnet.codeplex.com/wikipage?title=How%20to%20extend%20Spring.Northwind%20to%20generate%20different%20Northwind%20reports)

### Sample reports generated using Aspose.Cells

* [Products Catalog](https://asposespringnet.codeplex.com/wikipage?title=Products%20Catalog%20using%20Aspose.Cells)
* [Customer Labels](https://asposespringnet.codeplex.com/wikipage?title=Customer%20Labels%20using%20Aspose.Cells)
* [Products by Category](https://asposespringnet.codeplex.com/wikipage?title=Products by Category using Aspose.Cells)

### Sample reports generated using Aspose.Pdf

* [Products Catalog](https://asposespringnet.codeplex.com/wikipage?title=Products%20Catalog%20using%20Aspose.Pdf)
* [Customer Labels](https://asposespringnet.codeplex.com/wikipage?title=Customer%20Labels%20using%20Aspose.Pdf)

* [Products by Category](https://asposespringnet.codeplex.com/wikipage?title=Products%20by%20Category%20using%20Aspose.Pdf)

## What is SpringAir?
The SpringAir sample application demonstrates a selection of Spring.NET&rsquo;s powerful features making a .NET programmer&rsquo;s life easier. It demonstrates the following features of Spring.Web ... [Continue reading  →](https://asposespringnet.codeplex.com/wikipage?title=What%20is%20SpringAir)

## What is Spring.NET NHibernate Northwind?
This Spring.NET NHibernate Northwind application uses the Northwind database and uses NHibernate to browse and edit customers. It is a simple application that directly uses the DAO layer in many use-cases, as it is doing nothing more than table maintenance ... [Continue reading  →](https://asposespringnet.codeplex.com/wikipage?title=What%20is%20Spring.NET%20NHibernate%20Northwind) 

## What is the use of Aspose .NET Products?
[Aspose](http://www.aspose.com) are file format experts and provide APIs and components for various file formats including MS Office, OpenOffice, PDF and Image formats. These APIs are available on a number of development platforms including .NET frameworks &ndash; the .NET frameworks starting from version 2.0 are supported. If you are a .NET developer, you can use Aspose&rsquo;s native .NET APIs in your .NET applications to process various file formats in just a few lines of codes. All the Aspose APIs don&rsquo;t have any dependency over any other engine. For example, you don&rsquo;t need to have MS Office installed on the server to process MS Office files .. [Continue reading  →](https://asposespringnet.codeplex.com/wikipage?title=What%20is%20the%20use%20of%20Aspose%20.NET%20Products)